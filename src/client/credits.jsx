import React from "react";

function Credits() {
    return (
        <div className="credits-background">
            <p>
                Created by{" "}
                <b>
                    <a href="mailto:caleb@calebfaith.com">Caleb Faith</a>
                </b>
                {" - "}
                using{" "}
                <a href="https://github.com/thibauts/pop-buffer">
                    Pop Buffers
                </a>{" "}
                and <a href="https://threejs.org/">THREE.js</a>
            </p>
            <p>Simple single color mesh with a directional light.</p>
            <p>
                The current pop buffer implementation doesn&apos;t allow for
                vertex attributes like normals or UVs but that can definitely be
                done.
            </p>
            <p>
                It also suffers from a performance stutter because THREE.js
                takes the vertices/indices in a different format than the pop
                buffer calculation but the pop buffer input format can be
                changed.
            </p>
        </div>
    );
}

export default Credits;
