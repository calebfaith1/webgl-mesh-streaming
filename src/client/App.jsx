import React, { Component } from "react";
import "./app.css";
import Info from "./info";
import { encode, decode } from "@pop-buffer";
import Credits from "./credits";

const FBXLoader = require("three-fbx-loader");
const THREE = require("three");
const { createWriteStream } = require("streamsaver");
const zlib = require("zlib");

export default class App extends Component {
    state = { level: 0, verts: 0 };

    popBuffer = undefined;
    scene = undefined;
    camera = undefined;
    renderer = undefined;
    truck = undefined;
    rotationX = 0;
    rotationY = 0;
    lastTime = 0;
    rotationSpeed = 0.3;

    componentDidMount() {
        this.createScene();

        window.addEventListener("resize", this.onWindowResize, false);

        // to create the .bin data files uncomment this below
        // this.loadFBX();

        this.loadFromData();
        this.animate();

        window.addEventListener("keydown", this.onKeyDown, false);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onWindowResize, false);
        window.removeEventListener("keydown", this.onKeyDown, false);
        cancelAnimationFrame(this.animate);
    }

    // updates the mesh data with the current pop buffer settings.
    updateModel = () => {
        const { level } = this.state;

        console.log("Display in scene ------------------------");

        const geom = new THREE.BufferGeometry();

        // either use the current pop buffer
        // or if we aren't rendering all loaded levels create a pop buffer with the selected levels.
        let pb;

        if (level === 0) {
            pb = this.popBuffer;
        } else {
            pb = {};
            pb.bounds = this.popBuffer.bounds;
            pb.levels = [];

            for (let i = 0; i < this.popBuffer.levels.length - level; i++) {
                pb.levels.push(this.popBuffer.levels[i]);
            }
        }

        console.log(`Current Level: ${level}`);

        this.setState({
            verts: pb.levels[pb.levels.length - 1].positions.length / 3
        });

        const mesh = decode(pb);

        if (mesh.positions.length === 0 || mesh.cells.length === 0) {
            return;
        }

        // we need to change the vertex format to the THREE.js buffer format
        console.log("Flattening...");
        const verts = new Float32Array(mesh.positions.length * 3);
        for (let i = 0; i < mesh.positions.length; i++) {
            const index = i * 3;
            // eslint-disable-next-line prefer-destructuring
            verts[index] = mesh.positions[i][0];
            // eslint-disable-next-line prefer-destructuring
            verts[index + 1] = mesh.positions[i][1];
            // eslint-disable-next-line prefer-destructuring
            verts[index + 2] = mesh.positions[i][2];
        }

        const indices = [];
        for (let i = 0; i < mesh.cells.length; i++) {
            const index = i * 3;
            // eslint-disable-next-line prefer-destructuring
            indices[index] = mesh.cells[i][0];
            // eslint-disable-next-line prefer-destructuring
            indices[index + 1] = mesh.cells[i][1];
            // eslint-disable-next-line prefer-destructuring
            indices[index + 2] = mesh.cells[i][2];
        }

        console.log("Flatten complete!");

        // add vertices to geometry
        geom.addAttribute("position", new THREE.BufferAttribute(verts, 3));
        geom.setIndex(indices);
        geom.computeVertexNormals();

        const material = new THREE.MeshStandardMaterial({ color: 0x2194ce });
        if (this.truck) {
            this.scene.remove(this.truck);
        }

        // add to the scene
        this.truck = new THREE.Mesh(geom, material);
        this.scene.add(this.truck);
        this.camera.position.z = 30;

        console.log("Display in scene complete...");
    };

    // loads a local .fbx file and encodes the vertice and indice data. Writes this data to file.
    // Won't work on server - only for proof of concept
    loadFBX = () => {
        const loader = new FBXLoader();

        loader.load(
            "/public/semi.fbx",
            fbx => {
                console.log(
                    "Mesh loading and encoding ---------------------------"
                );

                const mesh = fbx.children[0];
                const rawPositions = Array.prototype.slice.call(
                    mesh.geometry.attributes.position.array
                );

                const tris = [];
                const vectorisedPosition = [];

                console.log("Vectorising positions...");
                for (let i = 0; i < rawPositions.length; i += 3) {
                    vectorisedPosition.push([
                        rawPositions[i],
                        rawPositions[i + 1],
                        rawPositions[i + 2]
                    ]);
                }

                console.log("Creating tris...");
                for (let i = 0; i < vectorisedPosition.length; i += 3) {
                    tris.push([i, i + 1, i + 2]);
                }

                console.log("Encoding pop buffer...");
                this.popBuffer = encode(tris, vectorisedPosition, 16);
                console.log("Encoding Complete!");

                this.writeToFile("truck");

                this.updateModel();
            },
            undefined,
            err => {
                console.error(err);
            }
        );
    };

    // writes the pop buffer to file.
    writeToFile = name => {
        // TODO: Better encoding of data and fix janky file saving

        const delay = 250;

        setTimeout(this.writeBoundsToFile(name), delay, name);

        setTimeout(this.writeLevelsToFile, delay * 2, 0, name);
    };

    // writes the popBuffer.bounds to file
    writeBoundsToFile = name => {
        console.log("Saving bounds to file...");

        const json = JSON.stringify(this.popBuffer.bounds);
        const compressed = zlib.gzipSync(json);

        const stream = createWriteStream(`${name}_bounds.bin`);
        const writer = stream.getWriter();
        writer.write(compressed);
        writer.close();

        console.log("Bounds Saved!");
    };

    // writes the indexed level to file.
    writeLevelsToFile = (index, name) => {
        const delay = 250;

        console.log(`Saving level ${index} to file...`);

        const json = JSON.stringify(this.popBuffer.levels[index]);
        const compressed = zlib.gzipSync(json);

        const stream = createWriteStream(`${name}_level_${index}.bin`);
        const writer = stream.getWriter();
        writer.write(compressed);
        writer.close();

        if (index === this.popBuffer.levels.length - 1) {
            console.log("Saving to file complete!");
        } else {
            setTimeout(this.writeLevelsToFile, delay, index + 1, name);
        }
    };

    // function to call to load the pop buffer from server.
    loadFromData = () => {
        this.popBuffer = {};
        this.popBuffer.levels = [];
        this.loadBounds();
    };

    // loads the bounds field of the pop buffer.
    loadBounds = () => {
        const request = new XMLHttpRequest();
        request.open("GET", "/truck_data/truck_bounds.bin", true);
        request.responseType = "arraybuffer";
        request.send(null);
        request.onreadystatechange = () => {
            if (request.readyState === 4 && request.status === 200) {
                const data = new Uint8Array(request.response);
                const uncompressed = zlib.gunzipSync(Buffer.from(data.buffer));
                const decoder = new TextDecoder();
                const json = decoder.decode(uncompressed);
                this.popBuffer.bounds = JSON.parse(json);
                console.log(this.popBuffer.bounds);
                this.loadLevels(0);
            }
        };
    };

    // loads the specified level of the pop buffer.
    loadLevels = index => {
        const request = new XMLHttpRequest();
        request.open("GET", `/truck_data/truck_level_${index}.bin`, true);
        request.responseType = "arraybuffer";
        request.send(null);

        request.onreadystatechange = () => {
            if (request.readyState === 4 && request.status === 200) {
                const data = new Uint8Array(request.response);
                const uncompressed = zlib.gunzipSync(Buffer.from(data.buffer));
                const decoder = new TextDecoder();
                const json = decoder.decode(uncompressed);
                this.popBuffer.levels.push(JSON.parse(json));

                if (index < 15) {
                    this.updateModel();
                    this.loadLevels(index + 1);
                } else {
                    this.updateModel();
                }
            }
        };
    };

    // sets up the THREE.js scene.
    createScene = () => {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(
            90,
            window.innerWidth / window.innerHeight,
            1,
            1000
        );

        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.renderer.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(this.renderer.domElement);

        const ambientLight = new THREE.AmbientLight(0x2a3643);
        this.scene.add(ambientLight);

        const directionalLight = new THREE.DirectionalLight(0xffffff, 0.8);
        this.scene.add(directionalLight);
    };

    // basic render loop.
    animate = () => {
        requestAnimationFrame(this.animate);

        const dt = (performance.now() - this.lastTime) / 1000;
        this.lastTime = performance.now();

        // rotate the model
        if (this.truck) {
            this.rotationX += this.rotationSpeed * dt;
            this.rotationY += this.rotationSpeed * dt;
            this.truck.rotation.x = this.rotationX;
            this.truck.rotation.y = this.rotationY;
        }

        this.renderer.render(this.scene, this.camera);
    };

    // called when the window resizes to update render settings
    onWindowResize = () => {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(window.innerWidth, window.innerHeight);
    };

    // called when the user presses a key down.
    // used to adjust levels.
    onKeyDown = event => {
        const keycode = event.key;

        let { level } = this.state;

        switch (keycode) {
            case "ArrowDown":
            case "ArrowLeft":
                level -= 1;
                level = Math.max(0, level);
                break;
            case "ArrowUp":
            case "ArrowRight":
                level += 1;
                level = Math.min(level, 16);
                break;
            default:
                break;
        }

        // eslint-disable-next-line react/destructuring-assignment
        if (level !== this.state.level) {
            this.setState({ level });
            this.updateModel();
        }
    };

    // renders this react component.
    render() {
        const { level, verts } = this.state;

        return (
            <div className="app">
                <Info level={level} verts={verts} displayControls />
                <Credits />
            </div>
        );
    }
}
