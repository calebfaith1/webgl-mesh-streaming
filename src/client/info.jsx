import React from "react";
import PropTypes from "prop-types";

function Info({ level, verts, displayControls }) {
    let displayControlsHTML;
    if (displayControls) {
        displayControlsHTML = (
            <div>
                <hr />
                <p>Use Arrow Keys to change levels</p>
            </div>
        );
    }

    return (
        <div>
            <div className="info-background">
                <h2>WebGL - Mesh Streaming</h2>
                <h4>Desktop Proof Of Concept Demo</h4>
                {displayControlsHTML}
            </div>
            <div className="info-background">
                <p>Level: {level}</p>
                <p>Verts Added On This Level: {verts}</p>
            </div>
        </div>
    );
}

Info.propTypes = {
    level: PropTypes.number.isRequired,
    verts: PropTypes.number.isRequired,
    displayControls: PropTypes.bool.isRequired
};

export default Info;
