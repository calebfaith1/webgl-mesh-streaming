# WebGL Mesh Streaming

Created by Caleb Faith (caleb@calebfaith.com) as a proof of concept for mesh streaming.
Put together fairly quickly so it is a bit rough around the edges but it gets the job done.

Uses pop buffers and THREE.js to progressively stream (or load) a 3D mesh.

Most logic happens in /src/client/App.jsx so check that out.
